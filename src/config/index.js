import config from './config'
import prodConfig from './config.prod'

const getConfig = () => process.env.NODE_ENV === 'production' ? prodConfig : config

export default getConfig()