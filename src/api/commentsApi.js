import config from '../config'
import authApi from './authApi'
import 'whatwg-fetch'

async function postComment(postId, postDate, comment) {
	const token = await authApi.getToken()
	return fetch(`${config.commentServer}/post/${postId}/comments`, {
		method: 'POST',
		headers: {
			authorization: `Bearer ${token}`,
			'content-type': 'application/json'
		},
		body: JSON.stringify({postDate, comment})
	})
	.then(res => res.json())
}

export default {postComment}