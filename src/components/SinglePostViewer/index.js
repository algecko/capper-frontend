//@flow
import React from 'react'
import graphql from 'babel-plugin-relay/macro'
import {QueryRenderer} from 'react-relay'
import {withRouter} from 'react-router-dom'
import DocumentTitle from 'react-document-title'

import environment from '../../relayEnv'
import Post from '../Post'
import LoadingOverlay from '../shared/LoadingOverlay'

import './SinglePostViewer.css'

const isBase64 = (text: string): boolean => !!text.match(/^([A-Za-z0-9+/]{4})*([A-Za-z0-9+/]{4}|[A-Za-z0-9+/]{3}=|[A-Za-z0-9+/]{2}==)$/)

const parseId = (id: string): string =>
	!isBase64(id) ?
		'' :
		atob(id).toLowerCase().startsWith('post:') ?
			id :
			btoa(`Post:${id}`)

const buildTitle = (post) => post ? `[${post.author.name}] ${post.title}` : ''

const SinglePostViewer = ({match}) => {
	return <QueryRenderer
		environment={environment}
		query={graphql`
          query SinglePostViewerQuery($id:ID!){
						post(id:$id){
							title
							author{
								name
							}
							...Post_post
						}
				}
					`}
		variables={
			{
				id: match && match.params && match.params.postId ? parseId(match.params.postId) : null
			}
		}
		render={({error, props}) => {
			if (error) {
				return <div>{error.toString()}</div>
			}
			if (!props) {
				return <LoadingOverlay/>
			}
			return props.post ? <DocumentTitle title={buildTitle(props.post)}>
				<div className={'singlePostViewer'}><Post post={props.post}/></div>
			</DocumentTitle> : 'Post not found'
		}}
	/>
}


export default withRouter(SinglePostViewer)
