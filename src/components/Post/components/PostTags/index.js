// @flow
import React from 'react'
import graphql from 'babel-plugin-relay/macro'
import {createFragmentContainer} from 'react-relay'

import Tags from '../../../shared/Tags/index'

import type {PostTags_post} from './__generated__/PostTags_post.graphql'

type Props = {
	post: PostTags_post
}

const PostTags = ({post}: Props) => post.tags ?
	<Tags tags={post.tags.map(tag => tag ? tag.name : '').filter(tag => tag && tag.length > 0)}/> : null

export default createFragmentContainer(
	PostTags, {
		post: graphql`
	fragment PostTags_post on Post {
		tags{
			name
		}
	}`
	}
)