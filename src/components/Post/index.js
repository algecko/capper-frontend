import React, {useEffect} from 'react'

import classNames from 'classnames'
import graphql from 'babel-plugin-relay/macro'
import {createFragmentContainer} from 'react-relay'

import TextArea from '../shared/TextArea/index'
import MomentDate from '../shared/MomentDate/index'
import PostTags from './components/PostTags/index'
import LikeButton from '../shared/LikeButton/index'
import Link from '../shared/Link/index'
import {connect} from 'react-redux'
import actions from './actions'
import reducer from './reudcer'
import PostImgWithFallback from '../shared/PostImgWithFallback'
import postSizing from '../shared/postSizing'
import {Link as RouterLink} from 'react-router-dom'

import './Post.css'
import Button from '../shared/Button'
import Comments from '../Comments'

import postPrep from './postPrep'

const getCommentsButtonTitle = (comments) => {
	const NO_COMMENTS_TEXT = 'No Comments'
	if (!comments || !comments.edges)
		return NO_COMMENTS_TEXT
	const commentCount = comments.edges.length
	if (commentCount === 0)
		return NO_COMMENTS_TEXT

	return `${commentCount} Comment${commentCount !== 1 ? 's' : ''}`
}

const Post = ({post, liked, unlikePost, likePost, toggleComments, showComments, postComment, loggedInUser}) => {

	useEffect(() => {
		return function cleanup() {
			// prevents gifs from being loaded
			//if (window.stop !== undefined)
			//	window.stop()
		}
	})

	return (
		<div className="post">
			<div className="postHeader">
				<Link to={`/post/${post.id}`} className='postTitle'><h2 className="postTitle pink">{post.title}</h2></Link>
				<div className="right">
					{post.size ? <span className="size">{postSizing.getTextForSize(post.size)}</span> : null}
					<div>
						<Link to={`/reader?authors=${post.author.name}`}><h4
							className="postAuthor pink">{post.author.name}</h4>
						</Link>
						<h5 className="postDate pink"><MomentDate date={post.date} format="L"/></h5>
					</div>
				</div>
			</div>
			<div className="postContent">
				{!post.noImg ?
					<div className="image">
						<PostImgWithFallback
							originalSrc={post.imgUrl}
							capperSrc={postPrep.prepareImgUrl(post.id, post.imgUrl)}
							alt={post.title} imgDead={post.imgDead}
						/>
					</div> : null}
				<div className={classNames('postText', 'spaced', {'noImg': post.noImg})}>
					<TextArea text={postPrep.prepareText(post.id, post.text)} imgDead={post.imgDead}/>
					<PostTags post={post}/>
					<div className="postControls">
						<Button active={showComments} buttonPressed={() => toggleComments(post.id)}
										title={getCommentsButtonTitle(post.comments)}/>
						<LikeButton nrOfLikes={post.likes || 0} liked={liked}
												buttonPressed={() => liked ? unlikePost(post.id) : likePost(post.id)}/>
						{loggedInUser && loggedInUser.toLowerCase() === post.author.name.toLowerCase() ?
							<RouterLink to={`/postMgmt/${post.id}`}>
								<Button title={'✎'}/>
							</RouterLink> : null}
					</div>
				</div>
			</div>
			{showComments ? <Comments comments={post.comments}
																postComment={(comment) => postComment(post.id, post.date, comment)}
																loggedInUser={loggedInUser}/> : null}
		</div>)
}


const mapStateToProps = (state, ownProps) => {
	const myState = state[reducer.MODULE_NAME]
	const likes = myState.get('likes')
	const showComments = myState.get('showComments')
	const postId = ownProps && ownProps.post ? ownProps.post.id : null
	return {
		...ownProps,
		liked: likes && postId ? likes.toJS().includes(postId) : false,
		pageCount: myState.getIn(['postData', 'pageCount']),
		currentPage: myState.getIn(['postData', 'currentPage']),
		loggedInUser: state.auth.get('loggedInUser'),
		showComments: showComments ? showComments.toJS().includes(postId) : false
	}
}

export default createFragmentContainer(
	connect(mapStateToProps, actions)(Post), {
		post: graphql`
	fragment Post_post on Post {
		id
		title
		date
		author{name}
		imgUrl
		text
		likes
		noImg
		size
		imgDead
		comments(first:null) @connection(key:"Post_comments"){
			edges{
				cursor
			}
			...Comments_comments
		}
		...PostTags_post
	}`
	}
)