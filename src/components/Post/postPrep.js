//@flow
import config from '../../config/index'

const prepareImgUrl = (postId: string, url: string, index: number) => `${config.imgServer}/images/${postId}?url=${encodeURIComponent(url)}${typeof (index) !== 'undefined' ? `&seq=${index}` : ''}`

const prepareText = (postId: string, text: string) => {
	if (!text)
		return text
	return extractImages(text)
		.reduce((text: string, imgUrl: string, currentIndex: number) => text.replace(imgUrl, prepareImgUrl(postId, imgUrl, currentIndex)), text)
}

function extractImages(text: string): Array<string> {
	const extractReffedImg = (imgKey: string) => {
		const matches = text.match(new RegExp(`\\[${imgKey}]:.*`, 'g'))
		if (matches) {
			const ref = matches[0]
			return ref.substring(ref.indexOf(':') + 1, ref.length).trim().split(' ')[0]
		}
	}

	const trimTrailingBracket = (text: string) => text && text.endsWith(')') ? text.substring(0, text.length - 1).trim() : text

	const postImgLinks = (text.match(/!\[.*]\(.*\)/g) || [])
		.map(item => trimTrailingBracket(item.substring(item.indexOf('(') + 1, item.length).trim().split(' ')[0]));


	(text.match(/!\[.*]\[.*]/g) || [])
		.map(item => item.substring(item.lastIndexOf('[') + 1, item.lastIndexOf(']')))
		.forEach(imgKey => {
			const refedImg = extractReffedImg(imgKey)
			if (refedImg)
				postImgLinks.push(trimTrailingBracket(refedImg))
		})

	return postImgLinks
}


export default {
	prepareImgUrl,
	prepareText
}