import React, {Component} from 'react'
import './Reader.css'
import Post from '../../../Post'
import {withRouter} from 'react-router-dom'
import graphql from 'babel-plugin-relay/macro'
import {createRefetchContainer} from 'react-relay'
import {scrollSpy, animateScroll as scroll} from 'react-scroll'
import querystring from 'querystring'
import classNames from 'classnames'
import parseQuery from '../../../shared/queryParser'
import SearchBox from '../../../SearchBox'

import type {Reader_reader} from './__generated__/Reader_reader.graphql'

import Button from '../../../shared/Button'

type Props = {
	reader: Reader_reader
}

class Reader extends Component<Props> {
	constructor(props) {
		super(props)

		this.state = {
			showSidebar: false
		}
	}

	componentDidMount() {
		scrollSpy.update()
	}


	getNewRefetchParams(oldQs, newQs, search) {
		return (oldParams) => {
			const params = {
				...oldParams,
				after: null,
				top: null,
				before: null,
				last: null,
				dateRange: null,
				maxSize: null,
				minSize: null,
				...parseQuery(search)
			}

			//masking because I don't want to give the impression that we can search in the whole text
			params.text = params.title
			if (oldQs.sort !== newQs.sort) {
				params.after = null
				params.before = null
			}
			return params
		}
	}

	componentWillReceiveProps(newProps) {
		const oldQs = this.getDecodedQs(this.props)
		const newQs = this.getDecodedQs(newProps)
		const {relay} = this.props


		if (relay && ['before', 'after', 'top', 'last', 'authors', 'tags', 'sort', 'dateRange', 'rand', 'title', 'likedByMe', 'maxSize', 'minSize'].find(key => oldQs[key] !== newQs[key]))
			relay.refetch(this.getNewRefetchParams(oldQs, newQs, newProps.location.search), undefined,
				(err) => {
					if (!err) {
						scroll.scrollToTop()
						if ((!newQs.sort || newQs.sort === 'new') && newQs.dateRange)
							this.setState({cleanDateRange: true})
					}
				})
	}

	getDecodedQs(props = this.props) {
		return querystring.decode(props.location.search.slice(1))
	}

	modifyQuery(alteration) {
		const {history: {push}} = this.props
		const newQuery = alteration(this.getDecodedQs())
		Object.keys(newQuery).forEach(key => newQuery[key] === undefined || newQuery[key] === null ? delete newQuery[key] : '')
		push(`/reader?${querystring.encode(newQuery)}`)
	}

	previousPage = () => {
		const {reader: {posts: {pageInfo}}} = this.props
		const {cleanDateRange} = this.state
		this.modifyQuery((lastQuery) => ({
			...lastQuery,
			after: null,
			first: null,
			last: lastQuery.last || lastQuery.first,
			before: pageInfo.startCursor,
			dateRange: cleanDateRange ? null : lastQuery.dateRange
		}))
		this.setState({cleanDateRange: false})
	}

	nextPage = () => {
		const {reader: {posts: {pageInfo}}} = this.props
		const {cleanDateRange} = this.state
		this.modifyQuery((lastQuery) => ({
			...lastQuery,
			after: pageInfo.endCursor,
			first: lastQuery.last || lastQuery.first,
			last: null,
			before: null,
			dateRange: cleanDateRange ? null : lastQuery.dateRange
		}))
		this.setState({cleanDateRange: false})
	}

	render() {
		const {reader: {posts}} = this.props
		return (
			<div className={`postReader`}>
				<div className={'searchWrapper'}>
					<div className={classNames('searchArea', {saActive: this.state.showSidebar})}>
						<SearchBox embedded/>
					</div>
					<Button className={'expander'} title={this.state.showSidebar ? '<' : '🔍'}
									overwriteDefaultStyle
									buttonPressed={() => this.setState({showSidebar: !this.state.showSidebar})}/>
				</div>
				<div className="mainContent">
					<div className={'postList'}>
						{posts && posts.edges && posts.edges.length > 0 ?
							posts.edges.map(({node: post}) =>
								<Post key={post.id} post={post}/>)
							: 'No Posts found'}
					</div>
					<div>
						<Button className={'pageNavButton'} title={'Previous'} buttonPressed={this.previousPage}
										disabled={!this.state.cleanDateRange && (!posts || !posts.pageInfo || !posts.pageInfo.hasPreviousPage)}/>
						<Button className={'pageNavButton'} title={'Next'} buttonPressed={this.nextPage}
										disabled={!this.state.cleanDateRange && (!posts || !posts.pageInfo || !posts.pageInfo.hasNextPage)}/>
					</div>
				</div>
			</div>
		)
	}
}

export default withRouter(createRefetchContainer(
	Reader, {
		reader:
			graphql`fragment Reader_reader on PostList @argumentDefinitions(
		first: {type: "Int", defaultValue: null}
		before: {type: "String", defaultValue: null}
		last: {type: "Int", defaultValue: null}
		after: {type: "String", defaultValue: null},
		tags: {type: "[String]", defaultValue: null},
		authors: {type: "[String]", defaultValue: null},
		sort: {type: "SortModes", defaultValue: new},
		dateRange: {type: "String", defaultValue: null},
		text: {type: "String", defaultValue: null},
		likedByMe: {type: "Boolean", defaultValue: false}
		minSize: {type: "Int", defaultValue: null}
		maxSize: {type: "Int", defaultValue: null}
	){
      posts(
      	first: $first
      	before: $before
      	last: $last
    		after: $after,
        tags: $tags,
        authors: $authors,
        sort: $sort,
        dateRange: $dateRange,
        text: $text,
        likedByMe: $likedByMe,
        minSize: $minSize,
        maxSize: $maxSize
      ) {
      	edges{
      		node{
      			id
      			date
      			...Post_post
      		}
      	}
				pageInfo{
					startCursor
					endCursor
					hasNextPage
					hasPreviousPage
				}
      }
    }`
	},
	graphql`
      query ReaderPaginationQuery(
        $first: Int,
        $after: String,
        $last: Int,
        $before: String,
        $tags: [String],
        $authors: [String],
        $sort: SortModes,
        $dateRange: String,
        $text: String,
        $likedByMe: Boolean,
        $maxSize: Int
        $minSize: Int
      ) {
        reader: reader {
          ...Reader_reader @arguments(first: $first, before: $before, last: $last, after:$after, tags:$tags, authors:$authors, sort:$sort, dateRange:$dateRange, text:$text, likedByMe:$likedByMe, maxSize: $maxSize, minSize: $minSize)
        }
      }
    `
))