import React, {useState} from 'react'
import graphql from 'babel-plugin-relay/macro'
import {QueryRenderer} from 'react-relay'
import parseQuery from '../shared/queryParser'

import environment from '../../relayEnv'

import Reader from './components/Reader'
import LoadingOverlay from '../shared/LoadingOverlay'

const PostReader = ({location: {search}, match}) => {
	const [vars] = useState(() => parseQuery(search))
	return <QueryRenderer
		environment={environment}
		query={graphql`
          query PostReaderQuery($first: Int, $before: String, $last: Int, $after: String, $tags: [String], $authors: [String], $sort: SortModes, $dateRange: String, $title: String, $maxSize: Int, $minSize: Int){
					reader{
						...Reader_reader @arguments(first: $first, before: $before, last: $last, after:$after, tags:$tags, authors: $authors, sort: $sort, dateRange: $dateRange, text: $title, maxSize: $maxSize, minSize: $minSize)
					}
				}
					`}
		variables={vars}
		render={({error, props}) => {
			if (error) {
				return <div>{error.toString()}</div>
			}
			if (!props) {
				return <LoadingOverlay/>
			}
			return <Reader match={match} reader={props.reader}/>
		}}
	/>
}

export default PostReader
