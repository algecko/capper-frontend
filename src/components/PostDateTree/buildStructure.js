// @flow

type PostPerDate = {
	day: Date,
	postCount: number
}

type TreeNode = {
	name: string,
	toggled: ?boolean,
	children: ?Array<TreeNode>,
	parent: ?TreeNode
}

type GroupCriterion = {
	getGroupIdentifier: (PostPerDate) => string,
	getValue: (PostPerDate) => number,
	sort: ?(string, string) => number,
	childGroupCriterion: ?GroupCriterion
}

const group = (inputData: Array<PostPerDate>, groupCriterion: GroupCriterion, parent: ?TreeNode): Array<TreeNode> => {
	const groups = inputData
	.reduce((grouped, item) => {
		const groupId = groupCriterion.getGroupIdentifier(item)
		let group = grouped[groupId]

		if (!group) {
			group = {total: 0, children: [], value: groupCriterion.getValue(item)}
			grouped[groupId] = group
		}

		group.total += item.postCount
		group.children.push(item)

		return grouped
	}, {})

	return Object.keys(groups)
	.sort(groupCriterion.sort || undefined).map(key => {
		const node: TreeNode = {
			name: `${key} (${groups[key].total})`,
			toggled: undefined,
			children: undefined,
			parent: parent,
			value: groups[key].value
		}
		if (groupCriterion.childGroupCriterion)
			node.children = group(groups[key].children, groupCriterion.childGroupCriterion, node)
		return node
	})
}

const monthNames = ['January', 'February', 'March', 'April', 'May', 'June',
	'July', 'August', 'September', 'October', 'November', 'December'
]
export default (inputData: Array<PostPerDate>) => group(inputData, {
	getGroupIdentifier: (item) => item.day.getFullYear().toString(),
	getValue: (item) => item.day.getFullYear(),
	sort: (a, b) => parseInt(b, 10) - parseInt(a, 10),
	childGroupCriterion: {
		getGroupIdentifier: (item) => monthNames[item.day.getMonth()],
		getValue: (item) => item.day.getMonth() + 1,
		sort: (a, b) => monthNames.indexOf(b) - monthNames.indexOf(a),
		childGroupCriterion: {
			getGroupIdentifier: (item) => item.day.getDate().toString(),
			getValue: (item) => item.day.getDate(),
			sort: (a, b) => parseInt(b, 10) - parseInt(a, 10),
			childGroupCriterion: null
		}
	}
})