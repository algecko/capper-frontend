import React from 'react'
import graphql from 'babel-plugin-relay/macro'
import {QueryRenderer} from 'react-relay'

import environment from '../../relayEnv'

import buildStructure from './buildStructure'
import TreeView from '../shared/TreeView'

type Props = {
	onSelectChanged: ?(newValue: Array<number>) => void
}

const PostDateTree = ({onSelectChanged}: Props) => <QueryRenderer
	environment={environment}
	query={graphql`
          query PostDateTreeQuery{
					 postsPerDate{
						day
						postCount
					}
				}
					`}
	variables={{}}
	render={({error, props}) => {
		if (error) {
			return <div>{error.toString()}</div>
		}
		if (!props) {
			return <div>Loading!</div>
		}
		if (!props.postsPerDate)
			return <div/>

		return <TreeView
			onSelectChanged={onSelectChanged}
			data={buildStructure(props.postsPerDate.map(item => ({
				day: new Date(item.day),
				postCount: parseInt(item.postCount, 10)

			})))}/>
	}}
/>


export default PostDateTree
