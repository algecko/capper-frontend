import React from 'react'
import PropTypes from 'prop-types'
import './Tag.css'

import Link from '../Link'

const Tags = ({tags}) => (
	tags.length > 0 ?
		<div className={'tags'}>
			{tags
			.filter((value, index, self) => self.indexOf(value) === index)
			.map(tag => <Link key={tag} to={`/reader?tags=${tag}`}>
				<div className={'tag'}>{tag}</div>
			</Link>)}
		</div> : null
)

Tags.propTypes = {
	tags: PropTypes.arrayOf(PropTypes.string).isRequired
}

export default Tags