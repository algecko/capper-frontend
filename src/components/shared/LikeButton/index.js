import React from 'react'
import Button from '../Button'
import {FaThumbsUp} from 'react-icons/fa'
import PropTypes from 'prop-types'

const getTitle = (nrOfLikes, liked) => liked ? `Liked (${nrOfLikes})` : !nrOfLikes || nrOfLikes === 0 ? 'Like' : `${nrOfLikes} likes`

const LikeButton = ({buttonPressed, nrOfLikes, liked}) => (
	<Button active={liked} title={getTitle(nrOfLikes, liked)}
					icon={<FaThumbsUp/>}
					buttonPressed={buttonPressed}/>)

LikeButton.propTypes = {
	liked: PropTypes.bool,
	nrOfLikes: PropTypes.number,
	buttonPressed: PropTypes.func
}

export default LikeButton