import React from 'react'
import PropTypes from 'prop-types'
import CommonMark from 'commonmark'
import ReactRenderer from 'commonmark-react-renderer'
import PostImgWithFallback from '../../shared/PostImgWithFallback'
import './TextArea.css'

const parser = new CommonMark.Parser()
const renderer = (imgDead) => new ReactRenderer({
	softBreak: 'br',
	renderers: {
		code: (props) => props.literal,
		code_block: (props) => props.literal,
		image: (props) => (<PostImgWithFallback capperSrc={props.src} alt={props.alt} imgDead={imgDead}/>)
	}
})

const TextArea = ({text, imgDead}) => {
	return (
		<div className={'textArea'}>
			{renderer(imgDead).render(parser.parse(text))}
		</div>
	)
}

TextArea.propTypes = {
	text: PropTypes.string
}

export default TextArea