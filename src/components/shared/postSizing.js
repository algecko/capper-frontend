// @flow

import config from '../../config'

const {sizing} = config

const getTextForSize = (postSize: number): string =>
	sizing['' + Object.keys(sizing).map(key => parseInt(key)).sort((a, b) => a - b).find(a => a > postSize)]

const getSizeFromText = (text: string): number => parseInt(Object.keys(sizing).find(key => sizing[key].toLowerCase() === text.toLowerCase()))

const getDisplayableSizes = () => Object.keys(sizing).map(key => ({
	key: sizing[key].toLowerCase(),
	value: key < 2000000 ? `${sizing[key].toUpperCase()} (${key}W)` : 'Any'
})).reduce((acc, curr) => ({...acc, [curr.key]: curr.value}), {})

export default {getTextForSize, getSizeFromText, getDisplayableSizes}