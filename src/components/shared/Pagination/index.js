import React from 'react'
import ReactPaginate from 'react-paginate'
import PropTypes from 'prop-types'
import './Pagination.css'


const Pagination = (props) => (
	<ReactPaginate previousLabel="<"
								 nextLabel=">"
								 containerClassName="capPagination"
								 pageClassName="capPage"
								 pageLinkClassName="capPageLink"
								 previousLinkClassName="capPageLink"
								 nextLinkClassName="capPageLink"
								 pageRangeDisplayed={2}
								 marginPagesDisplayed={1}
								 disableInitialCallback
								 {...props}/>
)

Pagination.propTypes = {
	pageCount: PropTypes.number.isRequired,
	initialPage: PropTypes.number,
	forcePage: PropTypes.number,
	onPageChange: PropTypes.func
}

export default Pagination