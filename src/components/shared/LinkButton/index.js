//@flow

import React from 'react'
import classNames from 'classnames'

import './LinkButton.css'

type Props = {
	title: string,
	onClick: () => void,
	className?: string
}

const LinkButton = ({title, onClick, className}: Props) => <span className={classNames('linkButton', className)}
																																 onClick={() => onClick()}>{title}</span>

export default LinkButton