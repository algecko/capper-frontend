//@flow
import React from 'react'

type Props = {
	groupName: string,
	className: ?string,
	itemClassName: ?string,
	items: { [value: string]: string },
	selectedItem: string,
	onSelectChanged: (selectedItem: string) => void
}

const RadioGroup = ({className, itemClassName, items, groupName, selectedItem, onSelectChanged}: Props) => (
	<div className={className}>
		{Object.keys(items).map(itemKey => {
			return <span key={itemKey}>
				<input type={'radio'}
							 className={itemClassName}
							 name={groupName}
							 value={itemKey}
							 checked={itemKey === selectedItem}
							 onChange={(e) => onSelectChanged(e.target.value)}
				/>
				{` ${items[itemKey]}`}
				</span>

		})}
	</div>
)

export default RadioGroup