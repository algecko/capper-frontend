import React from 'react'
import PropTypes from 'prop-types'
import moment from 'moment'
import browserLoc from 'browser-locale'

const MomentDate = ({date, format, className}) => {
	return (
		<span className={className}>{moment(date).locale(browserLoc()).format(format)}</span>
	)
}

MomentDate.propTypes = {
	date: PropTypes.any.isRequired,
	format: PropTypes.string.isRequired,
	className: PropTypes.string
}

MomentDate.defaultProps = {
	format: 'date'
}

export default MomentDate