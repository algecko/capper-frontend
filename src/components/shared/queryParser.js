// @flow
import queryString from 'querystring'

export type ParsedQuery = {
	[string]: string,
	tags: Array<string>,
	authors: Array<string>,
	likedByMe: boolean
}

const NUMERIC_FIELDS = ['first', 'last', 'maxSize', 'minSize']

const parseNumerics = (params: { [string]: string }) =>
	Object.keys(params).reduce((agg, key) => ({
		...agg,
		[key]: NUMERIC_FIELDS.find(a => a === key) ? parseInt(params[key]) : params[key]
	}), {})



export default (search: string): ParsedQuery => {
	const params = queryString.parse(search.startsWith('?') ? search.slice(1) : search)

	return {
		...parseNumerics(params),
		tags: params.tags ? params.tags.split(/[,;]/) : null,
		authors: params.authors ? params.authors.split(/[,;]/) : null,
		likedByMe: params.likedByMe === 'true' || params.likedByMe === '1' || params.likedByMe === 1
	}
}

