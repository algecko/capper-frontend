import React, {useState} from 'react'
import PropTypes from 'prop-types'

import './NewPasswordInput.css'

const NewPasswordInput = ({onPWChanged}) => {
	const [password, setPassword] = useState('')
	const [retypedPassword, setRetypedPassword] = useState('')

	return (
		<div className={'newPWInput'}>
			<div className={'input'}>
				<label>Password</label>
				<input type="password" onChange={(evt) => {
					setPassword(evt.target.value)
					if (onPWChanged) onPWChanged(evt.target.value, evt.target.value === retypedPassword)
				}}/>
			</div>
			<div className={'input'}>
				<label>Retype Password</label>
				<input type="password"
							 onChange={(evt) => {
								 setRetypedPassword(evt.target.value)
								 if (onPWChanged) onPWChanged(password, evt.target.value === password)
							 }}/>
			</div>
			{password !== retypedPassword ?
				<div className={'loginError'}>
					Passwords do not match
				</div>
				: null}
		</div>)
}

NewPasswordInput.propTypes = {
	onPWChanged: PropTypes.func.isRequired
}

export default NewPasswordInput