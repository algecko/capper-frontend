'use strict'

export default {
	tree: {
		base: {
			listStyle: 'none',
			backgroundColor: 'transparent',
			margin: 0,
			padding: 0,
			color: 'white',
			fontFamily: 'inherit',
			fontSize: '0.9em'
		},
		node: {
			base: {
				position: 'relative'
			},
			link: {
				cursor: 'pointer',
				position: 'relative',
				padding: '0px 5px',
				display: 'block'
			},
			activeLink: {
				background: '#31363F'
			},
			toggle: {
				base: {
					position: 'relative',
					display: 'inline-block',
					verticalAlign: 'top',
					margin: '0.25em 0.2em 0 0',
					height: '1.2em',
					width: '1.2em'
				},
				wrapper: {
					position: 'absolute',
					top: '50%',
					left: '50%',
					margin: '-7px 0 0 -7px',
					height: '14px'
				},
				height: 14,
				width: 14,
				arrow: {
					fill: '#f000c2',
					strokeWidth: 0
				}
			},
			header: {
				base: {
					display: 'inline-block',
					verticalAlign: 'top',
					color: 'white'
				},
				connector: {
					width: '2px',
					height: '12px',
					borderLeft: 'solid 2px black',
					borderBottom: 'solid 2px black',
					position: 'absolute',
					top: '0px',
					left: '-21px'
				},
				title: {
					lineHeight: '24px',
					verticalAlign: 'middle'
				}
			},
			subtree: {
				listStyle: 'none',
				paddingLeft: '19px'
			},
			loading: {
				color: '#E2C089'
			}
		}
	}
};
