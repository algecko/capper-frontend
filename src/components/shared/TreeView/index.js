//@flow

import React from 'react'
import {Treebeard} from 'react-treebeard'
import theme from './theme'

type Node = {
	name: string,
	active: ?boolean,
	toggled: ?boolean,
	children: ?Array<Node>,
	parent: Node,
	value: number
}

type Props = {
	data: Array<Node>,
	onSelectChanged: (selectedValue: Array<number>) => void
}

type State = {
	cursor: ?Node,
	data: Array<Node>
}

class TreeView extends React.Component<Props, State> {
	constructor(props: Props) {
		super(props)
		this.state = {data: props.data, cursor: null}
	}

	static buildValueChain(node: Node, values: ?Array<number>): Array<number> {
		if (!node)
			return values || []
		if (!values)
			values = []

		values.push(node.value)
		return TreeView.buildValueChain(node.parent, values)
	}

	onToggle = (node: Node, toggled: boolean) => {
		const {cursor} = this.state

		if (cursor) {
			cursor.active = false
		}

		node.active = true
		if (node.children) {
			node.toggled = toggled
		}

		this.setState({cursor: node})

		this.props.onSelectChanged(TreeView.buildValueChain(node))
	}

	render() {
		return <Treebeard data={this.state.data}
											onToggle={this.onToggle}
											style={theme}
		/>
	}
}

export default TreeView