import React from 'react'
import SimpleMDE from 'react-simplemde-editor'
import 'easymde/dist/easymde.min.css'
import './Style.css'

const getOptions = () => ({
	autofocus: true,
	spellChecker: false
	// etc.
})

const MDEditor = ({value, onChange}) => (
	<SimpleMDE value={value} onChange={onChange} options={getOptions()}/>
)

export default MDEditor
