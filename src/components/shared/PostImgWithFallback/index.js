import React from 'react'
import qs from 'querystring'
import Img from 'react-image'


const PostImgWithFallback = ({originalSrc, capperSrc, alt, imgDead}) => {
	const loadOrder = []

	if (!imgDead)
		loadOrder.push(originalSrc || qs.parse(capperSrc.split('?')[1]).url)

	loadOrder.push(capperSrc)
	loadOrder.push('/img_not_found.png')

	return (
		<Img
			src={loadOrder}
			decode={false}
			loader={(<img src={loadOrder[0]} alt={alt}/>)}/>)
}


export default PostImgWithFallback