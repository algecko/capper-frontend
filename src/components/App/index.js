import React from 'react'
import PostReader from '../PostReader/index'
import {Route, Redirect, Switch} from 'react-router-dom'
import LoginPage from '../AuthControl/LoginPage'
import RegistrationPage from '../AuthControl/RegistrationPage'
import AuthenticatedRoute from '../shared/AuthenticatedRoute'
import PostManagement from '../PostManagement'
import UserPage from '../UserPage'
import WelcomePage from '../WelcomePage'
import '../App/App.css'
import AppHeader from '../AppHeader'
import LegacyRedirect from '../LegacyRedirect'
import SinglePostViewer from '../SinglePostViewer'
import SearchBox from '../SearchBox'
import DocumentTitle from 'react-document-title'

const App = () => {
	return (
		<DocumentTitle title={'Open TGC'}>
			<div className={'appWrapper'}>
				<AppHeader/>
				<Switch>
					<Route path="/reader" component={PostReader}/>
					<Route path="/search" component={SearchBox}/>
					<Route path="/posts/:page?" component={LegacyRedirect}/>
					<Route path="/authors/:author/posts/:page?" component={LegacyRedirect}/>
					<Route path="/tags/:tag/posts/:page?" component={LegacyRedirect}/>
					<Route path="/post/:postId" component={SinglePostViewer}/>
					<Route path="/login" component={LoginPage}/>
					<Route path="/register" component={RegistrationPage}/>
					<AuthenticatedRoute path={'/postmgmt/:postId?'} component={PostManagement}/>
					<AuthenticatedRoute path={'/myAccount'} component={UserPage}/>
					<Route path="/" component={WelcomePage}/>
					<Redirect from="*" to="/"/>
				</Switch>
			</div>
		</DocumentTitle>
	)
}

export default App