// @flow
import * as React from 'react'
import TagsInput from 'react-tagsinput'
import {withRouter} from 'react-router-dom'
import {connect} from 'react-redux'
import 'react-tagsinput/react-tagsinput.css'
import Button from '../shared/Button'
import LinkButton from '../shared/LinkButton'
import RadioGroup from '../shared/RadioGroup'
import type {LocationShape, RouterHistory} from 'react-router-dom'
import type {ParsedQuery} from '../shared/queryParser'
import PostDateTree from '../PostDateTree'

import parseQuery from '../shared/queryParser'
import queryString from 'querystring'
import postSizing from '../shared/postSizing'

import './SearchBox.css'

type CustomLoc = LocationShape & {
	prevPath?: string,
	prevSearch?: string
}

type Props = {
	location: CustomLoc,
	history: RouterHistory,
	embedded?: boolean,
	authenticated: boolean
}

type State = {
	searchTags: Array<string>,
	sortMode: string,
	onlyLikedPosts: boolean,
	sizeFilter: string,
	sizeOperator: string
}

const searchTagsFromQuery = (query: ParsedQuery): Array<string> => {
	return (query.authors || []).map(author => `@${author}`)
		.concat((query.tags || []).map(tag => `#${tag}`))
		.concat((query.dateRange ? [`+${query.dateRange}`] : []))
		.concat(query.title ? query.title.split(/[,;]/) : [])
}

const sizeFilterFromQuery = (query: ParsedQuery): Object => ({
	// we can assume that only either min or max size is given at any time
	sizeOperator: query.minSize ? 'GTE' : 'LTE',
	sizeFilter: postSizing.getTextForSize(query.maxSize || query.minSize) || 'xl'
})

class SearchBox extends React.Component<Props, State> {

	constructor(props: Props) {
		super(props)

		const query = this.getCurrentQuery(props)

		this.state = {
			searchTags: searchTagsFromQuery(query),
			sortMode: query.sort || 'new',
			onlyLikedPosts: query.likedByMe,
			...sizeFilterFromQuery(query)
		}
	}

	getCurrentQuery = (props = this.props) => {
		const {embedded, location} = props
		return parseQuery(embedded ? (location.search || '') : location.prevPath === '/reader' && location.prevSearch ? '' + location.prevSearch : '')
	}

	componentWillReceiveProps(nextProps, nextContext) {
		const query = this.getCurrentQuery(nextProps)
		const newTags = searchTagsFromQuery(query)
		const oldTags = this.state.searchTags

		const stateChange = {}

		if (newTags.length !== oldTags.length
			|| newTags.some(tag => !oldTags.includes(tag)))
			stateChange.searchTags = newTags

		if (this.state.sortMode !== query.sort)
			stateChange.sortMode = query.sort || 'new'

		if (this.state.onlyLikedPosts !== query.likedByMe)
			stateChange.onlyLikedPosts = query.likedByMe

		if (Object.keys(stateChange).length > 0)
			this.setState({...this.state, ...stateChange})
	}

	handleChange = (searchTags: Array<string>) => {
		this.setState({
			...this.state,
			searchTags
		})
	}

	executeSearch = (searchTags: Array<string> = this.state.searchTags) => {
		const push = this.props && this.props.history ? this.props.history.push : null
		const pathname = !this.props.embedded ? 'reader' : this.props && this.props.location ? this.props.location.pathname : null
		if (!push || !pathname)
			return

		const {sizeOperator, sizeFilter} = this.state

		const authors = searchTags.filter(tag => tag.startsWith('@'))
		const tags = searchTags.filter(tag => tag.startsWith('#'))
		const dateRange = searchTags.find(tag => tag.startsWith('+'))
		const titleTags = searchTags.filter(tag => !tag.match(/^[@#+]/))

		const prepareParam = (array: Array<string>) => array.map(item => item.slice(1)).join(',')

		const newQuery = {
			...this.getCurrentQuery(),
			sort: this.state.sortMode
		}

		if (!this.state.onlyLikedPosts)
			delete newQuery.likedByMe
		else
			newQuery.likedByMe = true

		if (dateRange)
			newQuery.dateRange = dateRange.slice(1)
		else
			delete newQuery.dateRange

		if (newQuery.sort === 'new')
			delete newQuery.sort

		if (newQuery.sort === 'random')
			newQuery.rand = Math.floor(Math.random() * 1000000)
		else
			delete newQuery.rand

		if (titleTags.length > 0)
			newQuery.title = titleTags.map(tt => tt.trim()).join(',')
		else
			delete newQuery.title

		delete newQuery.before
		delete newQuery.after

		tags.length > 0 ?
			newQuery.tags = prepareParam(tags) :
			delete newQuery.tags
		authors.length > 0 ?
			newQuery.authors = prepareParam(authors) :
			delete newQuery.authors

		delete newQuery.maxSize
		delete newQuery.minSize

		if (sizeFilter !== 'xl') {
			const filterValue = sizeFilter !== 'xl' ? postSizing.getSizeFromText(sizeFilter) : postSizing.getSizeFromText('l')
			if (sizeOperator === 'LTE')
				newQuery.maxSize = filterValue
			else
				newQuery.minSize = filterValue
		}

		push(`${pathname}?${queryString.stringify(newQuery)}`)

	}

	onDateChanged = (newDate: Array<number>) => {
		this.setDateRange(newDate.reverse().map(nr => nr.toString().padStart(2, '0')).join('/'))
	}

	static convertDateToUtcString = (date) => `${date.getUTCFullYear()}/${(date.getUTCMonth() + 1).toString().padStart(2, '0')}/${date.getUTCDate()}`

	setDateRange = (range: string) => {
		this.setState({
			searchTags: this.state.searchTags
				.filter(st => !st.startsWith('+'))
				.concat([`+${range}`])
		})
	}

	setDateRangeToday = () => {
		this.setDateRange(SearchBox.convertDateToUtcString(new Date()))
	}

	setDateRangeThisWeek = () => {
		const lastWeek = new Date()
		lastWeek.setDate(lastWeek.getDate() - 7)
		this.setDateRange(`${SearchBox.convertDateToUtcString(lastWeek)}-${SearchBox.convertDateToUtcString(new Date())}`)
	}

	setDateRangeThisMonth = () => {
		const lastMonth = new Date()
		lastMonth.setMonth(lastMonth.getMonth() - 1)
		this.setDateRange(`${SearchBox.convertDateToUtcString(lastMonth)}-${SearchBox.convertDateToUtcString(new Date())}`)
	}

	render() {
		const searchButton = <div className={'searchButton'}>
			<Button title={'Search'} buttonPressed={() => this.executeSearch()}/>
		</div>

		return <div className={'searchBox'}>
			<h3>Search</h3>
			<p className={'searchHint'}>Search for @author, #tags or title</p>
			<TagsInput
				value={this.state.searchTags}
				onChange={this.handleChange}
				onlyUnique
				maxTags={10}
				addOnPaste
				tagProps={{
					className: 'searchTag',
					classNameRemove: 'tagRemove'
				}}
				addOnBlur
				inputProps={{placeholder: ''}}
			/>
			{searchButton}
			{!this.props.authenticated ? null : <div>
				<h4>Liked</h4>
				<input type='checkbox' checked={this.state.onlyLikedPosts}
							 onChange={(e) => this.setState({...this.state, onlyLikedPosts: e.target.checked})}/> Only show posts
				I've liked</div>}
			<h4>Sort</h4>
			<RadioGroup items={{new: 'New', best: 'Best', random: 'Random'}}
									selectedItem={this.state.sortMode}
									onSelectChanged={(sortMode) => this.setState({...this.state, sortMode})}
									className={'radioGroup'}
									itemClassName={'radioButton'}
									groupName={'sortMode'}
			/>
			{searchButton}

			<h4>By Date</h4>
			<div className={'dateView'}>
				<div className={'timeRangeButtons'}>
					<LinkButton title={'today'} onClick={this.setDateRangeToday}/>
					<LinkButton title={'this week'} onClick={this.setDateRangeThisWeek}/>
					<LinkButton title={'this month'} onClick={this.setDateRangeThisMonth}/>
				</div>
				<PostDateTree onSelectChanged={this.onDateChanged}/>
			</div>
			{searchButton}

			<h4>By Size</h4>
			<div className={'sizeFilter'}>
				<RadioGroup items={postSizing.getDisplayableSizes()}
										selectedItem={this.state.sizeFilter}
										onSelectChanged={(sizeFilter) => this.setState({...this.state, sizeFilter})}
										className={'radioGroup'}
										itemClassName={'radioButton'}
										groupName={'sizeFilter'}
				/>
				<RadioGroup items={{LTE: 'and smaller', GTE: 'larger than'}}
										selectedItem={this.state.sizeOperator}
										onSelectChanged={(sizeOperator) => this.setState({...this.state, sizeOperator})}
										className={'radioGroup'}
										itemClassName={'radioButton'}
										groupName={'sizeOperator'}
				/>
			</div>
			{searchButton}
		</div>
	}
}

const mapStateToProps = (state, ownProps) => ({
	...ownProps,
	authenticated: (state.auth.get('loggedInUser') || '').length !== 0
})


export default withRouter(connect(mapStateToProps)(SearchBox))