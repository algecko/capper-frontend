import React, {useState} from 'react'
import classNames from 'classnames'

import Link from '../shared/Link'
import AuthControl from '../AuthControl'
import './AppHeader.css'
import logo from './logo.png'
import {FaBars} from 'react-icons/fa'
import {withRouter} from 'react-router-dom'

const AppHeader = ({location}) => {

	const [open, setOpen] = useState(false)
	const close = () => setOpen(false)
	const toggleOpen = () => setOpen(!open)

	return (
		<div className={classNames('topnav', {'open': open})} id="myTopnav">
			<div className={'main'}>
				<Link to={'/'} className={'menuItem'} onLinkClick={close}>
					<img className={'headerLogo'} src={logo} alt={logo}/>
				</Link>
				<div className={'menuExpanderContainer'}>
					<Link action={toggleOpen}
								className={'menuExpander'} onLinkClick={close}><FaBars/></Link>
				</div>
			</div>
			<Link to={'/reader'} className={'menuItem'} onLinkClick={close}>Reader</Link>
			<Link to={{pathname: '/search', prevPath: location.pathname, prevSearch: location.search}}
						className={'menuItem mobileOnly'}
						onLinkClick={close}>Search</Link>
			<Link to={'/postmgmt'} className={'menuItem'} onLinkClick={close}>Create</Link>
			<div className={'spacer'}/>
			<Link to={'/myAccount'} className={'menuItem'} onLinkClick={close}>Profile</Link>
			<AuthControl className={'menuItem'} onLinkClick={close}/>
		</div>
	)
}

export default withRouter(AppHeader)