import React from 'react'
import {withRouter} from 'react-router-dom'
import {connect} from 'react-redux'
import reducer from './reducer'
import actions from './actions'
import LoadingOverlay from '../shared/LoadingOverlay'

import './PostManagement.css'
import PostEditor from './PostEditor'
import graphql from 'babel-plugin-relay/macro'
import {QueryRenderer} from 'react-relay'
import environment from '../../relayEnv'

const PostManagement = ({match, loggedInUser, savePost, saving, savingError, history: {push}}) => {
	return <div className={'postManagement'}>
		{saving ? <LoadingOverlay/> : null}
		<QueryRenderer
			environment={environment}
			query={graphql`
          query PostManagementQuery($id:ID!){
						post(id:$id){
							id,
							date,
							title,
							text,
							imgUrl,
							tags{
								name
							},
							author{
								name
							}
						}
					}
					`}
			variables={
				{
					id: match && match.params && match.params.postId ? match.params.postId : btoa(`Post:1`)
				}
			}
			render={({error, props}) => {
				if (error) {
					return <div>{error.toString()}</div>
				}
				if (!props) {
					return <LoadingOverlay/>
				}
				if (props.post && props.post.author.name.toLowerCase() !== loggedInUser.toLowerCase())
					return <div>No Permission to edit</div>
				return <PostEditor post={props.post} errorMessage={savingError} onSave={(post) => savePost(post, push)}/>
			}}/>
	</div>
}

const mapStateToProps = (state, ownProps) => {
	const myState = state[reducer.MODULE_NAME]
	return ({
		...ownProps,
		saving: myState.get('saving'),
		savingError: myState.get('savingError'),
		loggedInUser: state.auth.get('loggedInUser')
	})
}


export default withRouter(connect(mapStateToProps, actions)(PostManagement))