import React from 'react'
import PropTypes from 'prop-types'
import './PostEditor.css'
import Button from '../../shared/Button'
import MDEditor from '../../shared/MDEditor'

const preparePost = (post) => {
	if (!post)
		return {}
	const ret = {...post}

	ret.author = ret.author.name
	ret.tags = ret.tags.map(tag => tag.name).join(',')
	return ret
}

class PostEditor extends React.Component {
	constructor() {
		super()
		this.state = {
			id: undefined,
			date: new Date(),
			title: '',
			text: '',
			tags: '',
			imgUrl: '',
			noImg: false
		}
	}

	saveDisabled = () => {
		const post = this.state
		return !post || !post.title || post.title.length === 0 ||
			!post.text || post.text.length === 0 ||
			((post.imgUrl || '').length === 0 && !post.noImg)
	}

	componentDidMount(): void {
		this.setState({...this.state, ...preparePost(this.props.post)})
	}

	render() {
		const {onSave, errorMessage} = this.props
		return (
			<div className={'postCreator'}>
				<h2>{this.state.title.length === 0 ? 'New Post' : this.state.title}</h2>
				<div className={'input'}>
					<label>Title</label>
					<input type={'text'} value={this.state.title}
								 onChange={(evt) => this.setState({...this.state, title: evt.target.value})}/>
				</div>
				<div className={'input'}>
					<label>Image</label>
					<input type={'text'} value={this.state.imgUrl}
								 onChange={(evt) => this.setState({...this.state, imgUrl: evt.target.value})}
								 readOnly={this.state.noImg}/>
					<span className={'infoLabel'}>For now only links to other image hosters are supported. If you want to use an image from your hard disk please upload it to an image hoster like imgur and use the link they'll give you</span>
				</div>
				{!this.state.noImg ?
					<img className={'imgPreview'}
							 src={this.state.imgUrl}
							 alt={`Url doesn't seem to be valid as it can't be loaded`}/> : null}
				<div className={'checkInput'}>
					<input type="checkbox"
								 checked={this.state.noImg}
								 onChange={(e) => this.setState({...this.state, noImg: e.target.checked})}/>
					<label>I will include one ore more images in the actual post and don't want to the image on the left to be
						displayed.</label>
				</div>
				<div className={'input'}>
					<label>Tags</label>
					<input type={'text'} value={this.state.tags}
								 onChange={(evt) => this.setState({...this.state, tags: evt.target.value})}/>
					<span className={'infoLabel'}>Comma separated list of tags</span>
				</div>
				<div className={'input fill'}>
					<label>Text</label>
					<MDEditor value={this.state.text} onChange={(newText) => this.setState({...this.state, text: newText})}/>
				</div>
				{errorMessage ? <div className={'error'}>{errorMessage}</div> : null}
				<div className={'controls'}>
					<Button title={'Save'} buttonPressed={() => {
						if (onSave) onSave(this.state)
					}} disabled={this.saveDisabled()}/>
				</div>
			</div>
		)
	}
}

PostEditor.propTypes = {
	onSave: PropTypes.func,
	errorMessage: PropTypes.string,
	post: PropTypes.object
}

export default PostEditor