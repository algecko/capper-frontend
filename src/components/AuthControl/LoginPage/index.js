import React, {useState} from 'react'
import {connect} from 'react-redux'
import {Link} from 'react-router-dom'

import './LoginPage.css'

import Button from '../../shared/Button'
import LoadingOverlay from '../../shared/LoadingOverlay'
import actions from '../actions'
import reducer from '../reducer'

const LoginPage = ({loggingIn, error, login, location, history: {push}}) => {
	const [username, setUsername] = useState('')
	const [password, setPassword] = useState('')

	const doLogin = () => !login ? null : login(username, password, location.referrer, push)

	const loginEnabled = (username || '').length > 0 && (password || '').length > 0
	const handleKeyDown = (e) => e.keyCode === 13 && loginEnabled ? doLogin() : null

	return (
		<div className={'loginPage'}>
			{loggingIn ? <LoadingOverlay/> : null}
			<h3 className={'pink'}>Login</h3>
			<div className={'loginBox'}>
				<div className={'input'}>
					<label>Username</label>
					<input type="text" className={'userNameBox'} onChange={(evt) => setUsername(evt.target.value)}
								 onKeyDown={handleKeyDown}/>
				</div>
				<div className={'input'}>
					<label>Password</label>
					<input type="password" onChange={(evt) => setPassword(evt.target.value)}
								 onKeyDown={handleKeyDown}/>
				</div>
				{error ?
					<div className={'loginError'}>
						{error}
					</div> : null}
			</div>
			<Button title={'Log in'} buttonPressed={doLogin}
							disabled={!loginEnabled}/>
			<div className={'spacer'}/>
			<div className={'registerBox'}>
				<label>Not registered yet? </label>
				<Link to={'/register'}>
					<Button title={'Register'}/>
				</Link>
			</div>
		</div>
	)
}

const mapStateToProps = (state, ownProps) => {
	const myState = state[reducer.MODULE_NAME]
	return {
		...ownProps,
		loggingIn: myState.get('loggingIn', false),
		error: myState.get('loginError')
	}
}

export default connect(mapStateToProps, actions)(LoginPage)