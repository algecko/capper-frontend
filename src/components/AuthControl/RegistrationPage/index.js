import React, {useState} from 'react'
import {connect} from 'react-redux'
import {withRouter} from 'react-router-dom'

import './RegistrationPage.css'

import Button from '../../shared/Button'
import LoadingOverlay from '../../shared/LoadingOverlay'
import NewPasswordInput from '../../shared/NewPasswordInput'
import actions from '../actions'
import reducer from '../reducer'

const RegistrationPage = ({register, loggingIn, error, history: {push}}) => {

	const [username, setUsername] = useState('')
	const [password, setPassword] = useState('')
	const [pwValid, setPwValid] = useState(false)

	return (
		<div className={'registrationPage'}>
			{loggingIn ? <LoadingOverlay/> : null}
			<h3 className={'pink'}>Registration</h3>
			<div className={'registrationBox'}>
				<div className={'input'}>
					<label>Username</label>
					<input type="text" className={'userNameBox'} onChange={(evt) => setUsername(evt.target.value)}/>
				</div>
				<NewPasswordInput onPWChanged={(newPW, valid) => {
					setPassword(newPW)
					setPwValid(valid)
				}}/>
				{error ?
					<div className={'loginError'}>
						{error}
					</div> : null}
			</div>
			<Button title={'Register'} buttonPressed={() => register(username, password, push)} disabled={
				!username || username.length === 0 || !password || password.length === 0 || !pwValid
			}/>
		</div>
	)
}


const mapStateToProps = (state, ownProps) => {
	const myState = state[reducer.MODULE_NAME]
	return {
		...ownProps,
		loggingIn: myState.get('loggingIn', false),
		error: myState.get('loginError')
	}
}

export default withRouter(connect(mapStateToProps, actions)(RegistrationPage))