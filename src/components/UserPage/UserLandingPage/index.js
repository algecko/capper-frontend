import React from 'react'
import {connect} from 'react-redux'
import {withRouter} from 'react-router-dom'
import Link from '../../shared/Link/index'
import './UserLandingPage.css'

const UserLandingPage = ({loggedInUser, match}) => {
	return (<div className={'userMgmt'}>
		<h3 className={'pink'}>Hello {loggedInUser}</h3>
		<div>
			<Link to={`/reader?authors=${loggedInUser}`}>View my posts</Link>
			<Link to={`${match.path}/changePassword`}>Change password</Link>
		</div>
	</div>)
}

const mapStateToProps = (state, ownProps) => ({
	...ownProps,
	loggedInUser: state.auth.get('loggedInUser')
})

export default withRouter(connect(mapStateToProps)(UserLandingPage))