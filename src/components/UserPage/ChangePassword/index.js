import React, {useState} from 'react'
import {withRouter} from 'react-router-dom'
import PropTypes from 'prop-types'
import NewPasswordInput from '../../shared/NewPasswordInput'
import Button from '../../shared/Button'

import './ChangePassword.css'

const ChangePassword = ({errorMessage, onChangePassword, history:{push}}) => {
	const [oldPw, setOldPw] = useState('')
	const [newPw, setNewPw] = useState('')
	const [newPwValid, setNewPwValid] = useState(false)
	return (
		<div className={'changePassword'}>
			<h3 className={'pink'}>Change Password</h3>
			<div className={'newPWInput'}>
				<div className={'input'}>
					<label>Current Password</label>
					<input type="password" value={oldPw} onChange={(e) => setOldPw(e.target.value)}/>
				</div>
				<NewPasswordInput onPWChanged={(newPw, valid) => {
					setNewPw(newPw)
					setNewPwValid(valid)
				}}/>
			</div>
			{((errorMessage || '').length > 0) ?
				<div className={'pwChangeError'}>{errorMessage}</div> : null}
			<div className={'changePWControls'}>
				<Button title={'Change Password'}
								buttonPressed={() => {
									if (onChangePassword) onChangePassword(oldPw, newPw, push)
								}}
								disabled={(oldPw || '').length === 0 || (newPw || '').length === 0 || !newPwValid}/>
			</div>
		</div>)
}

ChangePassword.propTypes = {
	onChangePassword: PropTypes.func.isRequired,
	errorMessage: PropTypes.string
}

export default withRouter(ChangePassword)