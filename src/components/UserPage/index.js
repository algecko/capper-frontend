import React from 'react'
import {Route, Switch} from 'react-router-dom'
import {connect} from 'react-redux'
import {withRouter} from 'react-router-dom'

import '../App/App.css'

import actions from './actions'
import LandingPage from './UserLandingPage'
import ChangePassword from './ChangePassword'

import reducer from './reudcer'

const UserPage = ({match, changePassword, changePwError}) => {
	return (
		<Switch>
			<Route path={`${match.path}/changePassword`}
						 render={() => <ChangePassword onChangePassword={changePassword} errorMessage={changePwError}/>}/>
			<Route path={match.path} component={LandingPage}/>
		</Switch>
	)
}

const mapStateToProps = (state, ownProps) => {
	const myState = state[reducer.MODULE_NAME]
	return ({
		...ownProps,
		changePwError: myState.get('changePwError')
	})
}

export default withRouter(connect(mapStateToProps, actions)(UserPage))

