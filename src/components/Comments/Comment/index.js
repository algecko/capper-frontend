// @flow
import React from 'react'
import MomentDate from '../../shared/MomentDate'
import graphql from 'babel-plugin-relay/macro'
import {createFragmentContainer} from 'react-relay'
import './Comment.css'

import type {Comment_comment} from './__generated__/Comment_comment.graphql'

type Props = {
	comment: Comment_comment
}

const Comment = ({comment: {author, date, comment}}: Props) => (
	<div className={'postComment'}>
		<div className={'pink'}>
			<span className={'commentAuthor'}>{author.name}</span>
			<span className={'commentDate'}><MomentDate date={date} format="L LT"/></span>
		</div>
		<div className={'commentContent'}>{comment}</div>
	</div>
)

export default createFragmentContainer(
	Comment,
	{
		comment: graphql`fragment Comment_comment on Comment {
		author{
			name
		}
		date
		comment
	}`
	}
)