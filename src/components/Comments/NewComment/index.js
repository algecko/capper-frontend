import React, {useState} from 'react'
import PropTypes from 'prop-types'

import './NewComment.css'
import Button from '../../shared/Button'

const NewComment = ({postComment}) => {
	const [newComment, setNewComment] = useState('')

	const doPostComment = () => {
		if (postComment) postComment(newComment)
		setNewComment('')
	}

	return (
		<div className={'newComment'}>

		<textarea className={'newCommentText'} value={newComment}
							onChange={(evt) => setNewComment(evt.target.value)}/>
			<Button className={'postCommentButton'} title={'Leave Comment'} buttonPressed={doPostComment}/>
		</div>)
}

NewComment.propTypes = {
	newCommentValue: PropTypes.string,
	postComment: PropTypes.func
}

export default NewComment