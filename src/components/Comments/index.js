// @flow

import React from 'react'
import Comment from './Comment/index'
import NewComment from './NewComment/index'
import './Comments.css'
import graphql from 'babel-plugin-relay/macro'
import {createFragmentContainer} from 'react-relay'

import type {Comments_comments} from './__generated__/Comments_comments.graphql'

type Props = {
	comments: Comments_comments,
	newCommentText: string,
	postComment: () => void,
	loggedInUser: string
}

const Comments = ({comments, newCommentText, postComment, loggedInUser}: Props) => {
	const commentEdges = comments ? comments.edges || [] : []
	return (
		<div className={'postCommentWrapper'}>
			<h4 className={'commentsHeader pink'}>Comments</h4>
			<div className={'postComments'}>
				{commentEdges.length === 0 ? <div>Be the first to comment!</div> : null}
				{commentEdges.map(edge => edge && edge.node ?
					<Comment key={`${edge.node.author.name}:${edge.node.date.toString()}`}
									 comment={edge.node}/> : null)}
			</div>
			{loggedInUser && loggedInUser !== '' ?
				<NewComment newCommentValue={newCommentText} postComment={postComment}/> :
				<div>Please log in to leave a comment</div>}
		</div>)
}

export default createFragmentContainer(
	Comments,
	{
		comments: graphql`fragment Comments_comments on CommentConnection {
			edges{
				node{
					author{name}
					date
					...Comment_comment
				}
			}
		
	}`
	}
)