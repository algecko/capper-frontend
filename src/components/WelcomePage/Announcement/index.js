import React from 'react'
import MomentDate from '../../shared/MomentDate'
import {FaThumbtack} from 'react-icons/fa'
import TextArea from '../../shared/TextArea'
import classNames from 'classnames'

import './Announcement.css'

const Announcement = ({announcement}) => (
	<div className={classNames('announcement', {sticky: announcement.sticky})}>
		<div className={'announcementHeader pink'}>
			{announcement.sticky ? (<FaThumbtack/>) : null}By <span
			className={'announcementAuthor'}>{announcement.author}</span>

			<MomentDate date={announcement.date} format={'L'} className={'announcementDate'}/>
		</div>
		<div className={'announcementText'}>
			<TextArea text={announcement.text}/>
		</div>
	</div>
)


export default Announcement