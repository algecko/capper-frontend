import React from 'react'
import Announcement from './Announcement'
import './WelcomePage.css'
import Button from '../shared/Button'

import announcements from './announcements'

const sortFunction = (a, b) => {
	if (a.sticky && !b.sticky)
		return -1
	if (b.sticky && !a.sticky)
		return 1
	return new Date(b.date).getTime() - new Date(a.date).getTime()
}

const WelcomePage = ({history}) => {
	return (
		<div className={'welcomePage'}>
			<h2 className={'pink'}>Welcome to the new home of TG captions</h2>
			<div className={'staticWelcome'}>
				Open TGC follows the tradition of opentgcaptions.com and is a place for anybody to create and enjoy TG caps.
				<br/><br/>
				Feedback and ideas are always appreciated. You can reach my <a href={'mailto:albert.gecko@gmail.com'}>via
				mail</a> or on <a
				href="https://www.reddit.com/r/opentgcaptions/comments/8heiyk/a_new_otgc_archive_and_potential_successor/">this
				reddit thread</a>
				<br/><br/>
			</div>
			<Button className={'hugeButton'} active={true} buttonPressed={() => history.push('/posts')}
							title={'Check out the Reader'}/>
			<div className={'announcements'}>
				<h4 className={'pink'}>Announcements</h4>
				{announcements
				.sort(sortFunction)
				.map((ann, i) => <Announcement key={i} announcement={ann}/>)}
			</div>
		</div>
	)
}

export default WelcomePage