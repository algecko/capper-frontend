export class SessionExpiredError extends Error {
	constructor(message, ...params) {
		super(message, ...params)
		this.message = message || 'Session expired'
	}
}
